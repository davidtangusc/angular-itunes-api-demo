* pushing data onto array for previous searches
* ng-click

### Lesson plan

* review 2 way data binding (use diagram), views/templates, controller
* review angular's internal event system used to check if data has changed
* review directives
* review angular structure - modules, controllers, built-in services

* recreate app from last week using boilerplate and display a list of songs for an artist
* create custom service for itunes api
* trigger search based on user input using a form instead of page load
	* create form
	* ng-submit and methods on $scope
	* ng-hide for showing placeholder "Please make a search"
* previous searches
	* pushing data into an array of previous search terms
	* ng-switch statements like Handlebars if blockers


If there is time...

* favoriting songs
	* ng-click
	* passing data from ng-repeat to ng-click function

var app = angular.module('itunes-search-app', []);

app.run(function() {
	
});

app.controller('SongsSearchController', function($scope, iTunes, DoughnutChart) {
	$scope.previousSearches = [];
	$scope.favoriteSongs = [];

	$scope.search = function() {
		console.log($scope.artistSearch);

		$scope.loading = true;

		iTunes.search($scope.artistSearch).then(function(response) {
			var songs = response.data.results;
			var priceBreakdown = iTunes.getPriceStats(songs);

			$scope.songs = songs;
			$scope.loading = false;
			$scope.previousSearches.push($scope.artistSearch);
			$scope.artistSearch = '';

			DoughnutChart.create([
				{
					value: priceBreakdown._99,
					color: "#F38630"
				},
				{
					value : priceBreakdown._129,
					color : "#E0E4CC"
				}
			]);

		});
	};

	$scope.favorite = function(song) {
		$scope.favoriteSongs.push(song);
	};
});

app.service('iTunes', function($http) {
	this.search = function(artist) {
		var url = 'https://itunes.apple.com/search?term=' + artist + '&callback=JSON_CALLBACK';
		var promise = $http.jsonp(url);

		return promise;
	};

	this.getPriceStats = function(songs) {
		var prices = { _99: 0, _129: 0 };

		angular.forEach(songs, function(song) {
			var trackPrice = song.trackPrice;

			if (trackPrice === 0.99) {
				prices._99++;
			} else {
				prices._129++;
			}
		});

		return prices;
	};
});

app.service('DoughnutChart', function() {
	var chartDiv = document.getElementById('chart');

	this.create = function(data) {
		var canvas = document.createElement('canvas');
		var ctx;

		canvas.width = 500;
		canvas.height = 200;
		chartDiv.innerHTML = '';
		chartDiv.appendChild(canvas);
		ctx = canvas.getContext("2d");

		new Chart(ctx).Doughnut(data);
	};
});
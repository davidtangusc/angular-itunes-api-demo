angular-itunes-api-demo
=======================

A sample angular application using the iTunes API. It demonstrates a handful of common Angular directives, using $http for JSONP calls, creating custom services, and structuring your code using modules, controllers, and custom services.

There is a starter folder that doesn't contain any of the code if you want to try it from scratch.
